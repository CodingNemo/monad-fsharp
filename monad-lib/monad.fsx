﻿open System.Data
type Maybe<'a> = 
        Nothing 
        | Just of 'a


module Maybe =
    
    let ret x = Just x

    let bind x f = 
        match x with
        | Nothing -> Nothing
        | Just v -> f v

    type MaybeBuilder() = 
        member this.Bind(f,x)= bind f x
        member this.Return(x) = ret x

    let compute = MaybeBuilder()



type Result<'a> = 
        NotFound
        | Found of 'a

module Result =
    
    let ret x = Found x

    let bind x f = 
        match x with
        | NotFound -> NotFound
        | Found v -> f v

    type ResultBuilder() = 
        member this.Bind(x,f) = bind x f
        member this.Return(x) = ret x

    let compute = ResultBuilder()


type OptionBuilder() = 
    member this.Bind(x,f) = Option.bind f x
    member this.Return(x) = Some x

let option = OptionBuilder()

type Person = { Name: string; Father: Person option; Mother: Person option } 
       
module Genealogy = 
    let (>>=) f x = Maybe.bind f x
    let ret = Just
    let not = Nothing
    let compute = Maybe.compute

    let father x = match x.Father with
                   | Some f -> ret f
                   | None -> not

    let mother x = match x.Mother with
                   | Some m -> ret m
                   | None -> not


    let maternalGrandMother x = x |> mother >>= mother

    let maternalGrandFather x = x |> mother >>= father

    let bothGrandFathersOld x = x |> father >>= 
                                    (fun dad -> father dad >>= 
                                                (fun gf1 -> mother x >>= 
                                                            (fun mom -> father mom >>= 
                                                                        (fun gf2 -> ret (gf1, gf2)))))

    let bothGrandFathers x = compute {
        let! dad = father x
        let! gf1 = father dad
        let! mom = mother x
        let! gf2 = father mom
        return (gf1, gf2)
    }

let adam:Person = { Name= "adam"; Father= None; Mother= None }

let eve:Person = { Name= "eve"; Father= None; Mother= None }

let francis:Person = { Name= "francis"; Father= None; Mother= None }

let lola:Person = { Name= "lola"; Father= None; Mother= None }

let john:Person = { Name = "john"; Father= Some adam; Mother= Some eve}

let jane:Person = { Name = "jane"; Father= Some francis; Mother= Some lola}

let bob:Person = { Name= "bob"; Father= Some john; Mother= Some jane }

let gfs = Genealogy.bothGrandFathers bob

let bobGf = Genealogy.maternalGrandFather bob

let janeGfs = Genealogy.maternalGrandFather jane